require 'uri'
require 'net/http'
require 'openssl'
require 'httparty'


class TranslatedFile

    attr_accessor :to, :from, :text, :translation
    
    def initialize(to, from, text)
        @to = to
        @from = from
        @text = text
    end

    def translate
        url = URI("https://rapidapi.p.rapidapi.com/translate?to=#{to}&api-version=3.0&from=#{from}&profanityAction=NoAction&textType=plain")
            
        http = Net::HTTP.new(url.host, url.port)            
        http.use_ssl = true         
        http.verify_mode = OpenSSL::SSL::VERIFY_NONE
        
        request = Net::HTTP::Post.new(url)          
        request["content-type"] = 'application/json'        
        request["x-rapidapi-host"] = 'microsoft-translator-text.p.rapidapi.com'         
        request["x-rapidapi-key"] = 'f4a8443524msh779b6d95a15f094p157213jsn125f2a0fa7eb'
        request.body = "[\n    {\n        \"Text\": \"#{text}\"\n    }\n]"
        response = http.request(request)            
        trans1 = /t":".*?"/.match(response.read_body).to_s          
        trans2 = /[^t\"\:].*/.match(trans1).to_s
        @translation = trans2
        print 'translated: ' + '"' + trans2
    end

    def append_file
        time = Time.now
        file_name = time.strftime('%d-%m-%y_%H:%M')
        File.open("#{file_name}", 'a') do |line|
            line.write('translated: ' + '"' + @translation + "\n")
            puts ''
        end
    end
end
