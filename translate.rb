require 'uri'
require 'net/http'
require 'openssl'
require_relative 'translated_file'

print "translate to: "
trans_to = gets.chomp.to_s
print "translate from: "
trans_from = gets.chomp.to_s
puts ''
print "message: "
trans_msg = gets.chomp.to_s

text = TranslatedFile.new(trans_to, trans_from, trans_msg)
text.translate
text.append_file